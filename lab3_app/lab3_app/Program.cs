﻿namespace lab3_app
{
    class Program
    {
        static void Main(string[] args)
        {
            string directoryPath = @"E:\labs\СТМП\lab3";

            FileSystemWatcher watcher = new FileSystemWatcher(directoryPath);

            watcher.EnableRaisingEvents = true;

            watcher.Created += OnChanged;
            watcher.Deleted += OnChanged;
            watcher.Renamed += OnRenamed;

            Console.WriteLine($"Monitoring directory: {directoryPath}");
            Console.WriteLine("Press 'q' to quit.");

            while (Console.ReadKey().Key != ConsoleKey.Q) { }

            watcher.Dispose();
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
            LogToFile($"File: {e.FullPath} {e.ChangeType}");
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            Console.WriteLine($"File: {e.OldFullPath} renamed to {e.FullPath}");
            LogToFile($"File: {e.OldFullPath} renamed to {e.FullPath}");
        }

        private static void LogToFile(string message)
        {
            string logFilePath = @"E:\labs\СТМП\lab3\log.txt";

            using (StreamWriter writer = File.AppendText(logFilePath))
            {
                writer.WriteLine($"{DateTime.Now} - {message}");
            }
        }

    }
}